import threading
import datetime
import socket
import time
import sys


class boostSoc:
  '''
       Constructor, Getters, and Deconstructor
  '''
  def __init__(self, ip, port, frequency=0.05):
    self.__ip = ip
    self.__client = True
    if(self.__ip == ''):
      self.__client = False
    self.__port = port
    self.__connected = False
    self.__listening = False
    self.__connectionKillFlag = False
    self.__frequency = frequency

    self.__connectionThread = threading.Thread(target=self.__ConnectionManagement_DoWork, daemon=True) #args=(1,),
    self.__connectionThread.start()
    self.__serialThread = threading.Thread(target=self.__SerialManagement_DoWork, daemon=True)#args=(1,), 
    self.__serialThread.start()
    self.__connectedSocket = None

    self.__inMessages = []
    self.__inMessageLock = threading.Lock()
    self.__outMessages = []
    self.__outMessageLock = threading.Lock()


  def setFreq(self, newFrequency):
    self.__frequency = newFrequency

  def getConnected(self):
    return self.__connected

  def getListening(self):
    return self.__listening

  # Closes current socket connection ##  ToDo this should be a full deconstructor that ensures both the serial and connection thread are terminated
  def closeSocket(self):
    self.__connectedSocket.close()
    self.__connectionKillFlag = True
    print("Socket closed")

  '''   
      Socket Management           
  ''' 
  # This actually might not be needed, python is a higher level langauge with garbage collect and stuff
  #   But needs to be researched before taken out
  def __cleanupSoc(self):
    self.__connectionThread = None
    self.__serialThread = None
    self.__connectedSocket = None
    
  #
  def __ConnectionManagement_DoWork(self):
    while (not self.__connectionKillFlag):
      if(not self.__listening and not self.__connected):
        self.__cleanupSoc()
        self.__connectedSocket = self.__InitializeConnection()
      # if(!autoReconnect){break;} // end connection thread after one attempt
    time.sleep(0.500)


  # Creates either the client or server depending on what is selected
  def __InitializeConnection(self):
    self.__listening = True
    if(self.__client):
      conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      try:
        conn.connect((self.__ip, self.__port))
        self.__connected = True
        print("Connected to new server")
      except socket.error:
        self.__connected = False    
      self.__listening = False
      return conn
    else: #if(not client):
      conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      conn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
      conn.bind(('', self.__port))
      conn.listen(1)
      server, addr = conn.accept()
      print("Connected to a new client")
      self.__connected = True
      self.__listening = False
      return server

  '''   
       Serial Management           
  '''   
  def __SerialManagement_DoWork(self):  
    while not self.__connectionKillFlag:
      time.sleep(self.__frequency)
      if  not self.__connected or self.__listening:
        continue
      # try sending message queue or heartbeat
      #Yeah I didn't feel like looking up locks but maybe lock outMessages and inMessages
      self.__outMessageLock.acquire()
      if len(self.__outMessages) > 0:
        messages = ""
        for message in self.__outMessages:
          messages = messages + message + ";"
        self.__outMessages.clear()
      else:
        messages = ";"
      self.__outMessageLock.release()
      try:
        msgBytes = messages.encode('utf-8')
        self.__connectedSocket.send(msgBytes)      
      except:
        print("Failed to send message")
        self.__connected = False      
      # try reading incoming messages
      try:
        
        messages = self.__connectedSocket.recv(1024).decode('utf-8')
        # need to throw in a loop to catch buffer overflow               
        messageArray = messages.split(';')
        self.__inMessageLock.acquire()
        for message in messageArray:
          if message != "":
            self.__inMessages.append(message)
        self.__inMessageLock.release()
        
        #self.__inMessages.append(messages)
      except:
        message = "Error getting message"

  # Sends message on current socket connection
  def sendMessage(self, message):
    self.__outMessageLock.acquire()
    self.__outMessages.append(message)
    self.__outMessageLock.release()

  def getMessages(self): 
    if not self.__connected:
      return ""
    rtnMessages = self.__inMessages.copy()
    self.__inMessages.clear()
    return rtnMessages

  #
  # ToDo:
  #   getNextMessage(): FIFO
  #   getLastMessage(): LIFO
  #   
  
  def getUnreadMessage(self):
    return len(self.__inMessages)

