import sys
import time
import socket
import threading
from pyBoostSoc import boostSoc

ip = ''


def readMessage():
  connected = True
  while connected:
    AllMessages = soc.getMessages()
    for message in AllMessages:
      print("\r" + "Friend: " + str(message))
    time.sleep

## Setup socket
while True:
  print("Do you wanna be client or server (c/s)")
  socketType = input()

  if (socketType == "s"):
    client = False
    print("Which port would you like to use? ")
    port = int(input())
    print("Creating new server...")
    soc = boostSoc('',port)
    #conn = soc.startSocket()
    break
  elif (socketType == "c"):
    client = True
    print("What ip do you wanna connect to?")
    ip = input()
    print("What port would you like to use?")
    port = int(input())
    print("Creating new client")
    soc = boostSoc(ip,port)
    #conn = soc.startSocket()
    break
  else:
    print("Command not recognized")
  time.sleep(.25)

## Grab latest message
readThread = threading.Thread(target=readMessage)#,args=(1,))
readThread.start()
## Send messages 
while True:
  message = input("Me: ")
  if (message == "close"):
    soc.closeSocket()
    break
  soc.sendMessage(message)
  time.sleep(.05)
